"use strict";

var congruence = {};

// Остаток от деления с переводом отрицательного в положительное
congruence.Mod = function(num, m){
	// Прибавляем достаточно большое число, кратное модулю
	if(num < 0) num += Math.round(num/m + 1) * m;
	return num % m;
};

// НОД массива чисел
congruence.GCD = function(e){
	function gcd(a,b){ return (!b) ? a : gcd(b, a % b); }
	return gcd(e[0], (!e[2]) ? e[1] : this.GCD(e.slice(1)));
};

// Упрощение произведения по модулю
// Например: a*b*c*d ≡ x (mod m) 
congruence.List = function(list, m){
	if(list.length == 0) return 1;
	// Если число в массиве list больше m то находим его остаток от деления
	for(var i in list) list[i] = this.Mod(list[i], m);
	if(list.length == 1) return list[0];
	// Массим может оказаться длинным. 
	// Во избежание выхода за пределы (int) разбиваем массив на пары чисел
	return (list[0] * this.List(list.slice(1), m)) % m;
};

// Разложение в цепную дробь дроби a/b
congruence.ContinuedFraction = function(a, b){
	// Адекватность прежде всего =^_^=
	// Рекурсивная функция, аргументы возвращаются строками
	// [1,2,3,3,3,6] == "1|2|3|3|3|6"
	function cf(a, b){ return (a % b == 0) ? a : Math.floor( a/b ) + '|' + cf(b, a % b); }
	return (cf(a, b)).split('|');
}

// P_i число для cf — (ContinuedFraction некоторой дроби)
congruence.P_i = function(cf, i){
	return (i < 0) ? 0 : ((i == 0) ? 1 : (cf[i-1] * this.P_i(cf, i - 1) + this.P_i(cf, i - 2)));
}

// Решение простого уравнения Ax ≡ B (mod m)
congruence.SolveSimple = function(A, B, m){
	// Если A B m имеют общий множитель, на него нудно делить
	var gcd = this.GCD([A, B, m]);
	B = B/gcd; 
	m = m/gcd;
	
	// Может оказаться, что A больше мдуля (A % m)
	var cf = this.ContinuedFraction(m, (A/gcd) % m);
	var solution = this.List([(cf.length%2 == 0) ? -1 : 1, this.P_i(cf, cf.length - 1), B], m), results = [];
	for (var i = 0; i < gcd; i++) results.push(solution + i * m);
	return results;
}

// Нахождение обратного к A по модулю m числа A'
// A * A' ≡ 1 (mod m)





