$(function(){
	// Disable enter key - 13
	var input = $("#input"), output = $("#output"), buttons = $("#buttons li a");
	try { disableKeyPressEvent('13'); } catch(error) { try { console.log(error); } catch(error) {} }

	$(document).keypress(function(e){
		if(e.keyCode == 13) move(); // ENTER
	});
	input.click(function(){
		if($(this).html() == '←') $(this).html('');
	});
	buttons.click(function(){
		input.focus().html( $(this).data('id') );
		move();
	});
	
	// Запуск функции
	function evaluate(){
		var code = input.html();
		try { var resp = eval('[' + code + ']'); resp = '<pre>' + JSON.stringify(resp) + '</pre>'; } catch (e) { var resp = '<small>' + e + '</small>'; }
		output.html(resp);
	}
	
	// Сдвиг до следующей звёздочки	
	function move(){
		var name = input.html(), place = name.indexOf('•');
		if(place == -1) return evaluate();
		var range = document.createRange(), sel = window.getSelection();
		input.html(name.replace('•', ''));
		range.setStart(input[0].childNodes[0], place);
		range.collapse(true);
		sel.removeAllRanges();
		sel.addRange(range);
	}

	// Предотвращение переноса строки
	function disableKeyPressEvent(key) {
		if (!key) return false;
		$(document).keypress(function(e) {
			if((e.charCode || e.keyCode) == key) return false; 
		});
	}

});


