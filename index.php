<!DOCTYPE html>
<html manifest="math.appcache">
<head>
	<meta charset="utf-8">
	<title>Congruence</title>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon" href="touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="76x76" href="gui/img/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="gui/img/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="gui/img/touch-icon-ipad-retina.png">
	<link href="gui/css/ratchet.css" rel="stylesheet">
	<link href="gui/css/user.css" rel="stylesheet">
</head>
<body>

	<!-- Make sure all your bars are the first things in your <body> -->
	<header class="bar bar-nav">
		<h1 class="title">Теория чисел. Congruence.js</h1>
	</header>

	<div class="content">

		<div id="eval">
			<div id="input" class="box red" contenteditable="true">←</div>
			<div id="output" class="box normal">→ Строка ввода (красная) может быть использована как JavaScript консоль. 
			Для запуска программного кода используйте «Enter».
			Программы по теории чисел находятся в объекте «congruence». К ним можно получить быстрый доступ по кнопкам ниже</div>
		</div>
		
		<div id="buttons">
		<ul class="table-view">
			<li class="table-view-cell media">
				<a data-id="congruence.SolveSimple(•,•,•)">
				<div class="media-body">SolveSimple(A, B, m) 
				<p>Решение простого уравнения Ax ≡ B (mod m)</p></div>
				</a>
			</li>
			<li class="table-view-cell media">
				<a data-id="congruence.ContinuedFraction(•,•)">
				<div class="media-body">ContinuedFraction(a, b) 
				<p>Разложение в цепную дробь дроби a/b</p></div>
				</a>
			</li>
			<li class="table-view-cell media">
				<a data-id="congruence.GCD([•])">
				<div class="media-body">GCD([number, number, ...]) 
				<p>НОД массива чисел</p></div>
				</a>
			</li>
			<li class="table-view-cell media">
				<a data-id="congruence.Mod(•,•)">
				<div class="media-body">Mod(number, module) 
				<p>Остаток от деления с переводом отрицательного в положительное</p></div>
				</a>
			</li>
			<li class="table-view-cell media">
				<a data-id="congruence.List([•],•)">
				<div class="media-body">List([number, number, ...], module) 
				<p>Упрощение произведения по модулю<br/>Например: a*b*c*d ≡ x (mod m)</p></div>
				</a>
			</li>
			<li class="table-view-cell media">
				<a data-id="congruence.P_i([•],•)">
				<div class="media-body">P_i(ContinuedFraction, i) 
				<p>P<sub>i</sub> — i-ое число для ContinuedFraction некоторой дроби</p></div>
				</a>
			</li>
		</ul>
		</div>
	</div>

	<script src="app/congruence.js"></script>

	<script src="gui/js/jquery-2.1.0.min.js"></script>
	<script src="gui/js/makeHTML.js"></script>
	<script src="gui/js/ratchet.js"></script>

</body>
</html>